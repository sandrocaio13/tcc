import pandas as pd
import time
import numpy as np
import tensorflow as tf
import seaborn as sns
import matplotlib.gridspec  as gridspec
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE

start = time.time()
df = pd.read_csv('files/creditcard.csv')

print(df.head())
print(df.describe())
print(df.isnull().sum())

print('\nFraud')
print(df.Time[df.Class == 1].describe())
print('---------')
print('Normal')
print(df.Time[df.Class == 0].describe())

# f, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(12, 4))
#
# bins = 50
#
# ax1.hist(df.Time[df.Class == 1], bins=bins)
# ax1.set_title('Fraud')
#
# ax2.hist(df.Time[df.Class == 0], bins=bins)
# ax2.set_title('Normal')
#
# plt.xlabel('Time (in Seconds)')
# plt.ylabel('Number of Transactions')
# plt.show()
#
#
# print('\nFraud')
# print(df.Amount[df.Class == 1].describe())
# print('---------')
# print('Normal')
# print(df.Amount[df.Class == 0].describe())
#
# f, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(12, 4))
#
# bins = 30
#
# ax1.hist(df.Amount[df.Class == 1], bins=bins)
# ax1.set_title('Fraud')
#
# ax2.hist(df.Amount[df.Class == 0], bins=bins)
# ax2.set_title('Normal')
#
# plt.xlabel('Amount ($)')
# plt.ylabel('Number of Transactions')
# plt.yscale('log')
# plt.show()

df['Amount_max_fraud'] = 1
df.loc[df.Amount <= 2125.87, 'Amount_max_fraud'] = 0

# f, (ax1, ax2) = plt.subplots(2, 1, sharex='all', figsize=(12, 4))
#
# ax1.scatter(df.Time[df.Class == 1], df.Amount[df.Class == 1])
# ax1.set_title('Fraud')
#
# ax2.scatter(df.Time[df.Class == 0], df.Amount[df.Class == 0])
# ax2.set_title('Normal')
#
# plt.xlabel('Time (in seconds)')
# plt.ylabel('Amount')
# plt.show()

v_features = df.ix[:, 1:29].columns

# plt.figure(figsize=(12, 28 * 4))
# gs = gridspec.GridSpec(28, 1)
# for i, cn in enumerate(df[v_features]):
#     print(f'Maping: {i}')
#     ax = plt.subplot(gs[i])
#     sns.distplot(df[cn][df.Class == 1], bins=50)
#     sns.distplot(df[cn][df.Class == 0], bins=50)
#     ax.set_xlabel('')
#     ax.set_title(f'histogram of feature: {cn}')
# plt.show()
# plt.imsave('graphs/new_graph.png')

# elimina os recursos que possuem semelhança entre os dois tipos de classe
df = df.drop(['V28', 'V27', 'V26', 'V25', 'V24', 'V23', 'V22', 'V20', 'V15', 'V13', 'V8'], axis=1)

# com base nos gráficos acima, esses recursos são criados para identificar valores em que transações fraudulentas são
# mais comuns.
df['V1_'] = df.V1.map(lambda x: 1 if x < -3 else 0)
df['V2_'] = df.V2.map(lambda x: 1 if x > 2.5 else 0)
df['V3_'] = df.V3.map(lambda x: 1 if x < -4 else 0)
df['V4_'] = df.V4.map(lambda x: 1 if x > 2.5 else 0)
df['V5_'] = df.V5.map(lambda x: 1 if x < -4.5 else 0)
df['V6_'] = df.V6.map(lambda x: 1 if x < -2.5 else 0)
df['V7_'] = df.V7.map(lambda x: 1 if x < -3 else 0)
df['V9_'] = df.V9.map(lambda x: 1 if x < -2 else 0)
df['V10_'] = df.V10.map(lambda x: 1 if x < -2.5 else 0)
df['V11_'] = df.V11.map(lambda x: 1 if x > 2 else 0)
df['V12_'] = df.V12.map(lambda x: 1 if x < -2 else 0)
df['V14_'] = df.V14.map(lambda x: 1 if x < -2.5 else 0)
df['V16_'] = df.V16.map(lambda x: 1 if x < -2 else 0)
df['V17_'] = df.V17.map(lambda x: 1 if x < -2 else 0)
df['V18_'] = df.V18.map(lambda x: 1 if x < -2 else 0)
df['V19_'] = df.V19.map(lambda x: 1 if x > 1.5 else 0)
df['V21_'] = df.V21.map(lambda x: 1 if x > 0.6 else 0)

# criando uma nova feature para transações normais
df.loc[df.Class == 0, 'Normal'] = 1
df.loc[df.Class == 1, 'Normal'] = 0

# renomeando a classe para fraude
df = df.rename(columns={'Class': 'Fraud'})

print('\n')
print(df.Normal.value_counts())
print('------')
print(df.Fraud.value_counts())

pd.set_option("display.max_columns", 101)
print(df.head())

# criando frames para divisao de fraudes e normal
Fraud = df[df.Fraud == 1]
Normal = df[df.Normal == 1]

# setando x_train equal a 80% das transacoes fraudulentas
x_train = Fraud.sample(frac=0.8)
count_frauds = len(x_train)

# setando 80% das transacoes normais para treino
x_train = pd.concat([x_train, Normal.sample(frac=0.8)], axis=0)

# para teste, pega os resto
x_test = df.loc[~df.index.isin(x_train.index)]

# embaralhe os dados para o treinamento ser em ordem aleatória
x_train = shuffle(x_train)
x_test = shuffle(x_test)

# adiciona ou marca as feature em y_train e y_test
y_train = x_train.Fraud
y_train = pd.concat([y_train, x_train.Normal], axis=1)

y_test = x_test.Fraud
y_test = pd.concat([y_test, x_test.Normal], axis=1)

# ver depois
x_train = x_train.drop(['Fraud', 'Normal'], axis=1)
x_test = x_test.drop(['Fraud', 'Normal'], axis=1)

# checando as medidas e verificando se estar correto
print(len(x_train))
print(len(y_train))
print(len(x_test))
print(len(y_test))

'''
Devido ao desequilíbrio nos dados, a proporção funcionará como um sistema de ponderação igual para o nosso modelo.
Ao dividir o número de transações por aquelas que são fraudulentas, a proporção será igual ao valor que, quando multiplicado
pelo número de transações fraudulentas será igual ao número de transações normais.
Basta colocar: # of fraud * ratio = # of normal
'''

ratio = len(x_train) / count_frauds
y_train.Fraud *= ratio
y_test.Fraud *= ratio

# nome de todas as features em x_train
features = x_train.columns.values
print(features)

# transforme cada recurso em recursos para que ele tenha uma média de 0 e desvio padrão de 1;
# isso ajuda com o treinamento da rede neural.
for feature in features:
    mean, std = df[feature].mean(), df[feature].std()
    x_train.loc[:, feature] = (x_train[feature] - mean) / std
    x_test.loc[:, feature] = (x_test[feature] - mean) / std

print('Treinando a rede neural')

# divida os dados em tests dentro do conjunto de testes
splits = int(len(y_test) / 2)

inputX = x_train.values
inputY = y_train.values
inputX_valid = x_test.values[:splits]
inputY_valid = y_test.values[:splits]
inputX_tests = x_test.values[splits:]
inputY_tests = y_test.values[splits:]

# numeros de nos de entrada
input_nodes = 37

# o multiplicador mantém uma proporção fixa de nós entre cada camada.
multplier = 1.5

# numeros de nós em nas camadas de classificacao
hidden_nodes1 = 18
hidden_nodes2 = round(hidden_nodes1 * multplier)
hidden_nodes3 = round(hidden_nodes2 * multplier)

# porcentagem de nós durante a saida
pkeep = tf.placeholder(tf.float32)

# camada de entrada
x = tf.placeholder(tf.float32, [None, input_nodes])

# layer 1
W1 = tf.Variable(tf.truncated_normal([input_nodes, hidden_nodes1], stddev=0.15))
b1 = tf.Variable(tf.zeros([hidden_nodes1]))
y1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1)

# layer 2
W2 = tf.Variable(tf.truncated_normal([hidden_nodes1, hidden_nodes2], stddev=0.15))
b2 = tf.Variable(tf.zeros([hidden_nodes2]))
y2 = tf.nn.sigmoid(tf.matmul(y1, W2) + b2)

# layer 3
W3 = tf.Variable(tf.truncated_normal([hidden_nodes2, hidden_nodes3], stddev=0.15))
b3 = tf.Variable(tf.zeros([hidden_nodes3]))
y3 = tf.nn.sigmoid(tf.matmul(y2, W3) + b3)
y3 = tf.nn.dropout(y3, pkeep)

# layer 4
W4 = tf.Variable(tf.truncated_normal([hidden_nodes3, 2], stddev=0.15))
b4 = tf.Variable(tf.zeros([2]))
y4 = tf.nn.softmax(tf.matmul(y3, W4) + b4)

# camada de saida
y = y4
y_ = tf.placeholder(tf.float32, [None, 2])

# parametros
training_epochs = 10  # colocar depos 2000
training_dropout = 0.9
display_step = 1     # colocar 10 depois
n_samples = y_train.shape[0]
batch_size = 2048
learning_rate = 0.005

# funcao de custo: Cross Entropy
cost = -tf.reduce_sum(y_ * tf.log(y))

# nós vamos otimaze o modelo com AdamOptimizer
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

# previsão correta se o valor mais provável (Fraude ou Normal) do softmax for igual ao valor alvo.
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

accuracy_summary = []  # grava as acuracias plotadas
cost_summary = []  # grava os valores plotados
valid_accuracy_summary = []
valid_cost_summary = []
stop_early = 0

# Salve os melhores pesos para que eles possam ser usados ​​para fazer as previsões finais
# checkpoint = "location_on_your_computer / best_model.ckpt"
saver = tf.train.Saver(max_to_keep=1)

# inicializar o tensorflow
with tf.Session() as session:
    session.run(tf.global_variables_initializer())

    for epoch in range(training_epochs):
        for batch in range(int(n_samples / batch_size)):
            batch_x = inputX[batch * batch_size: (1 + batch) * batch_size]
            batch_y = inputY[batch * batch_size: (1 + batch) * batch_size]

            session.run([optimizer], feed_dict={x: batch_x, y_: batch_y, pkeep: training_dropout})

        # mostre apos 10 epochs
        if epoch % display_step == 0:
            train_accuracy, newCost = session.run([accuracy, cost], feed_dict={x: inputX,
                                                                               y_: inputY,
                                                                               pkeep: training_dropout})

            valid_Accuracy, valid_newCost = session.run([accuracy, cost], feed_dict={x: inputX_valid,
                                                                                     y_: inputY_valid,
                                                                                     pkeep: 1})

            print(f'Epochs: {epoch}, ' +
                  f'Acc: {train_accuracy}, ' +
                  f'Cost: {newCost}, ' +
                  f'Valid_acc: {valid_Accuracy} ' +
                  f'Valid_cost: {valid_newCost}')

            # salve os pesos, se as condicoes forem corretas
            # if epoch > 0 and valid_Accuracy > max(valid_accuracy_summary) and valid_Accuracy > 0.999:
            #     saver.save(session, checkpoint)

            # grava os resultados do modelo
            accuracy_summary.append(train_accuracy)
            cost_summary.append(newCost)
            valid_accuracy_summary.append(valid_Accuracy)
            valid_cost_summary.append(valid_newCost)

            # se o modelo não tiver uma melhora apos 15 logs, pare o treinamento
            if valid_Accuracy < max(valid_accuracy_summary) and epoch > 100:
                stop_early += 1
                if stop_early == 15:
                    break
                else:
                    stop_early = 0
    print('\n-----------')
    print('Otimizacao finalizada')
    print('-------------')

# plot acuracia e cost sumarios
f, (ax1, ax2) = plt.subplots(2, 1, sharex='all', figsize=(10, 4))

ax1.plot(accuracy_summary)  # em azul
ax1.plot(valid_accuracy_summary)  # em vermelho
ax1.set_title('Acuracia')

ax2.plot(cost_summary)
ax2.plot(valid_cost_summary)
ax2.set_title('Custo')

plt.xlabel('Epocas (x10)')
plt.show()

# recarrega os dados
tsne_data = pd.read_csv('files/creditcard.csv')

# set df2 igual as transacoes fraudulentas
df2 = tsne_data[tsne_data.Class == 1]
df2 = pd.concat([df2, tsne_data[tsne_data.Class == 0].sample(n = 10000)], axis=0)

# melhorar a capacidade de treinamento da TSNE
standard_scaler = StandardScaler()
df2_std = standard_scaler.fit_transform(df2)

# set y igual aos valores marcados
y = df2.ix[:,-1].values
tsne = TSNE(n_components=2, random_state=0)
x_test_2d = tsne.fit_transform(df2_std)

# # construindo o gráfico
# color_map = {0: 'red', 1: 'blue'}
# plt.figure()
# for idx, cl in enumerate(np.unique(y)):
#     plt.scatter(x = x_test_2d[y==cl, 0],
#     y = x_test_2d[y==cl, 1],
#     c = color_map[idx],
#     label = cl)

# plt.xlabel('X in t-SNE')
# plt.ylabel('Y in t-SNE')
# plt.legend(loc='upper left')
# plt.title('t-SNE visualization of test data')
# plt.show()

#Set df_used to the fraudulent transactions' dataset.
df_used = Fraud

#Add 10,000 normal transactions to df_used.
df_used = pd.concat([df_used, Normal.sample(n = 10000)], axis = 0)

#Scale features to improve the training ability of TSNE.
df_used_std = standard_scaler.fit_transform(df_used)

#Set y_used equal to the target values.
y_used = df_used.ix[:,-1].values

x_test_2d_used = tsne.fit_transform(df_used_std)

x_test_2d_used = tsne.fit_transform(df_used_std)

color_map = {1:'red', 0:'blue'}
plt.figure()
for idx, cl in enumerate(np.unique(y_used)):
    plt.scatter(x=x_test_2d_used[y_used==cl,0], 
                y=x_test_2d_used[y_used==cl,1], 
                c=color_map[idx], 
                label=cl)
plt.xlabel('X in t-SNE')
plt.ylabel('Y in t-SNE')
plt.legend(loc='upper left')
plt.title('t-SNE visualization of test data')
plt.show()

print(f'{int(time.time() - start)} seconds')
