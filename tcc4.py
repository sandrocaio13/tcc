import pandas as pd
import numpy as np
from scipy import stats
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_recall_curve, recall_score, classification_report, auc, roc_curve
from sklearn.metrics import precision_recall_fscore_support, f1_score
from sklearn.preprocessing import StandardScaler
from pylab import rcParams
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras import regularizers

# set sementes aleatórias e porcentagem dos dados de teste
RANDOM_SEED = 314
TEST_PCT = 0.2

# set estilo do grafico 
rcParams['figure.figsize'] = 14, 8.7 # Golden Mean
LABELS = ['Normal', 'Fraud']
col_list = ['denim blue', 'scarlet']
sns.set(style='white', font_scale=1.75, palette=sns.xkcd_palette(col_list),color_codes=False)

df = pd.read_csv('files/train_new.csv')
print(df.head(n=10))
print(df.shape)

# check para ver se algum valor é nulo
print(df.isnull().values.any())

# vamos contar quantos dados são Fraudes e quantos dados são normais
print(pd.value_counts(df['Class'], sort=True))

# vamos mostrar como os dados são desbalanceados, ou seja
# existe mais informação de um do que outro
# count_classes = pd.value_counts(df['Class'], sort=True)
# count_classes.plot(kind='bar', rot=0)
# plt.xticks(range(2), LABELS)
# plt.title('Números para a observação')
# plt.xlabel('Classes')
# plt.ylabel('Números de observações')
# plt.show()

# dividindo os dados em classes
normal_df = df[df.Class == 0]
fraud_df = df[df.Class == 1]

print(fraud_df.PAY_AMT6.describe())

# mostrarei aqui a comparacao entre normal e fraude por valores acima de 200 usd
# bins = np.linspace(2, 10000, 100)
# plt.hist(normal_df.PAY_AMT6, bins, alpha=1, density=True, label='Normal')
# plt.hist(fraud_df.PAY_AMT6, bins, alpha=0.6, density=True, label='Fraude')
# plt.legend(loc='upper right')
# plt.title('Comparações de valores por idade')
# plt.xlabel('Fraudes por idades')
# plt.ylabel('Porcentagem de transações')
# plt.show()

# # mostrarei aqui a comparacao entre normal e fraude por 48 horas
# bins = np.linspace(20, 80, 80)
# plt.hist(normal_df.Age, bins, alpha=1, density=True, label='Normal')
# plt.hist(fraud_df.Age, bins, alpha=0.6, density=True, label='Fraude')
# plt.legend(loc='upper right')
# plt.title('Comparações de valores por idade')
# plt.xlabel('Fraudes por idades')
# plt.ylabel('Porcentagem de transações')
# plt.show()

# vamos normalizar os dados
df_norm = df
df_norm['Age'] = StandardScaler().fit_transform(df_norm['Age'].values.reshape(-1, 1))
df_norm['PAY_AMT6'] = StandardScaler().fit_transform(df_norm['PAY_AMT6'].values.reshape(-1, 1))


train_x, test_x =  train_test_split(df_norm, test_size=TEST_PCT, random_state=RANDOM_SEED)
train_x = train_x[train_x.Class == 0] # pega as transações normais
train_x = train_x.drop(['Class'], axis=1) # retira a coluna Class

test_y = test_x['Class'] # salva a coluna Class para test
test_x = test_x.drop(['Class'], axis=1)

train_x = train_x.values # tranform in ndarray
test_x = test_x.values

print(train_x.shape)

nb_epochs = 100
batch_size = 128
input_dim = train_x.shape[1] # numeros de colunas == 30
encoding_dim = 14
hidden_dim = int(encoding_dim/2) # == 7
learning_rate = 1e-7

input_layer = Input(shape=(input_dim, ))
encoder = Dense(encoding_dim, activation='tanh', activity_regularizer=regularizers.l1(learning_rate))(input_layer)
encoder = Dense(hidden_dim, activation='relu')(encoder)
decoder = Dense(hidden_dim, activation='tanh')(encoder)
decoder = Dense(input_dim, activation='relu')(decoder)
autoencoder = Model(inputs=input_layer, outputs=decoder)

autoencoder.compile(metrics=['accuracy'], loss='mean_squared_error', optimizer='adam')
cp = ModelCheckpoint(filepath='auto_encoder_tcc4.h5', save_best_only=True, verbose=0)
tb = TensorBoard(log_dir='logs/', histogram_freq=0, write_graph=True, write_images=True)
# autoencoder = load_model('auto_encoder.h5')
history = autoencoder.fit(train_x, train_x, epochs=nb_epochs, batch_size=batch_size, shuffle=True, 
validation_data=(test_x, test_x), verbose=1, callbacks=[cp, tb]).history

# veremos agora a taxa de perda e acuracia
plt.plot(history['loss'], linewidth=2, label='Train')
plt.plot(history['val_loss'], linewidth=2, label='Test')
plt.legend(loc='upper right')
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epocas')
plt.show()
