from random import randint
from mpl_toolkits.axes_grid1 import make_axes_locatable

# import Tensorflow and numpy
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

# columns of file
COLUMNS = ['LIMIT_BAL', 'SEX', 'EDUCATION', 'MARRIAGE', 'AGE', 'PAY_0', 'PAY_2', 'PAY_3', 'PAY_4', 'PAY_5', 'PAY_6',
          'BILL_AMT1', 'BILL_AMT2', 'BILL_AMT3',
          'BILL_AMT4', 'BILL_AMT5', 'BILL_AMT6', 'PAY_AMT1', 'PAY_AMT2', 'PAY_AMT3', 'PAY_AMT4', 'PAY_AMT5', 'PAY_AMT6',
          'DEFAULT']

training_data_raw = np.genfromtxt('files/train.csv', delimiter=',')
holdout_data_raw = np.genfromtxt('files/holdout.csv', delimiter=',')

print('Iniciando a sessão do Tensorflow')
sess = tf.InteractiveSession()

# iniciando os numpy arrays com numeros aleatorios
train_data = np.empty((len(training_data_raw) - 2, 24))
holdout_data = np.empty((len(holdout_data_raw) - 2, 24))
train_data_result = np.empty((len(training_data_raw) - 2, 1))
holdout_data_result = np.empty((len(holdout_data_raw) - 2, 1))

# lembre que as duas primeiras linhas, sao rotulos, pule
training_row_index = 0
holdout_row_index = 0
csv_row_indxe = 0

# defina suas normalizacoes
nf_credit_amount = 250000
nf_payament_amount = 250000
nf_age = 60
nf_generic_factor = 7

# populando o np array com os valores do arquivo
for row in training_data_raw:
    if training_row_index == 0 or training_row_index == 1:
        training_row_index += 1
        continue
    j = 0
    # a ultima linha é o resultado
    for element in row[:-1]:
        # normalizando o dado se for necessario
        # if j == 0:
        #     j += 1
        if j == 0:
            train_data[training_row_index - 2][j] = float(element) / nf_credit_amount
        elif j == 4:
            train_data[training_row_index - 2][j] = float(element) / nf_age
        elif 10 < j < 24:
            train_data[training_row_index - 2][j] = float(element) / nf_payament_amount
        else:
            train_data[training_row_index - 2][j] = float(element) / nf_generic_factor
        j += 1
    # populando a saida
    train_data_result[training_row_index - 2][0] = row[j]
    training_row_index += 1

for row in holdout_data_raw:
    if holdout_row_index == 0 or holdout_row_index == 1:
        holdout_row_index += 1
        continue
    j = 0
    # a ultima linha é o resultado
    for element in row[:-1]:
        if j == 0:
            holdout_data[holdout_row_index - 2][j] = float(element) / nf_credit_amount
        elif j == 4:
            holdout_data[holdout_row_index - 2][j] = float(element) / nf_age
        elif 10 < j < 24:
            holdout_data[holdout_row_index - 2][j] = float(element) / nf_payament_amount
        else:
            holdout_data[holdout_row_index - 2][j] = float(element) / nf_generic_factor
        j += 1
    # populando a saida
    holdout_data_result[holdout_row_index - 2][0] = row[j]
    holdout_row_index += 1

print('Imprimindo data treinada')
print(train_data)

print('Imprimindo dados esperados')
print(holdout_data)

print('Imprimindo resultado dos dados')
print(train_data_result)


# Funções para inicializar pesos e bieses aleatoriamente para evitar a explosão do gradiente e do problema de gradiente
# decrescente

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


# iniciando o modelo
# criando a entrada do tensor de [infinita, 6] dimensao
x = tf.placeholder(tf.float32, [None, 24])

# criando pesoas e bias para o tensor com a primeira camada possuindo 6 entradas e 4 saidas
Wx = weight_variable([24, 20])
Bx = bias_variable([20])

# Defind the equation with activation function for the first layer to be RELU
hi = tf.nn.relu(tf.matmul(x, Wx) + Bx)

# Create weights and bias tensor variables for the second layer with 4 inputs and 1 output

Whi = weight_variable([20, 10])
bhi = bias_variable([10])

# Defind the equation with activation function for the first layer to be RELU

h1 = tf.nn.relu(tf.matmul(hi, Whi) + bhi)

# Create weights and bias tensor variables for the second layer with 4 inputs and 1 output

Wh1 = weight_variable([10, 1])
bh1 = bias_variable([1])

# Defind the equation with activation function for the first layer to be RELU

h2 = tf.nn.relu(tf.matmul(h1, Wh1) + bh1)

# Create weights and bias tensor variables for the second layer with 4 inputs and 1 output

Wh2 = weight_variable([16, 1])
bh2 = bias_variable([1])

# Defind the activation function for the second layer to simply output a number based on weights and biases

y = tf.nn.relu(tf.matmul(h1, Wh1) + bh1)

# Define loss and optimizer

# Estimated output is an infinite tensor of single dimension
y_ = tf.placeholder(tf.float32, [None, 1])

# Cross entropy is based on L2 loss. [(y-y_)exp2/2*input samples_count]. Reduce index 1 because 0 is just the index of the output tensor
cross_entropy = tf.reduce_sum(tf.pow(y - y_, 2), reduction_indices=[1]) / (len(train_data))  # L2 loss

# Learning rate control variable is 0.75. Found through iteration starting from a low value. Big value resulted in missed minima
train_step = tf.train.GradientDescentOptimizer(0.75).minimize(cross_entropy)

# Train
tf.global_variables_initializer().run()

train_index = 0

print('Iniciando o treinamento')

training_batch_size = 100

for i in range(100000):
    train_data_batch = np.empty((training_batch_size, 24))
    train_data_result_batch = np.empty((training_batch_size, 1))

    # seleciona 100 resultados e enriqueca os dados
    for batch_index in range(training_batch_size):
        train_index = randint(0, len(train_data) - 1)
        train_data_result_batch[batch_index][0] = train_data_result[train_index]
        row = train_data[train_index]
        j = 0
        for element in row:
            # if j == 4:
            #     element *= 100
            train_data_batch[batch_index][j] = element
            j += 1

    # print(train_data_batch)
    # print(train_data_result_batch)

    train_step.run({x: train_data_batch, y_: train_data_result_batch})

    if i % training_batch_size == 0:
        print(f'Interacao: {i}')

print('Treinamento completo')

holdout_index = 0

# Set the threshold for the default risk
default_risk_threshold = 0.20
TP = 0
FP = 0
TN = 0
FN = 0

# Calculate TP, TN, FP, FN

risk = [None] * len(holdout_data_result)

for i in range(len(holdout_data)):
    holdout_data_batch = np.empty((1, 24))

    row = holdout_data[i]

    j = 0
    for element in row:
        holdout_data_batch[0][j] = element
        j = j + 1

    # print('Holdout data = %s' % holdout_data[i])
    # print('Input = %s' % holdout_data_batch)

    get_risk = y
    risk_score = get_risk.eval({x: holdout_data_batch})
    risk[i] = risk_score[0][0]

    print('Predicted Risk = %s - Real Risk = %s' % (risk[i], holdout_data_result[i][0]))
    if ((risk[i] > default_risk_threshold) and (holdout_data_result[i][0] == 1)):
        TP = TP + 1
    elif ((risk[i] > default_risk_threshold) and (holdout_data_result[i][0] == 0)):
        FP = FP + 1
    elif ((risk[i] < default_risk_threshold) and (holdout_data_result[i][0] == 1)):
        FN = FN + 1
    elif ((risk[i] < default_risk_threshold) and (holdout_data_result[i][0] == 0)):
        TN = TN + 1

# lets calculate accuracy to be how many true positives we got right
precision = float(TP) / (TP + FP)
recall = float(TP) / (TP + FN)
accuracy = float(TP) / (TP + FN)
print(f'Accuracy = {accuracy} \nPrecision = {precision} \nRecall = {recall} \nTP = {TP} \nFP = {FP} \nTN = {TN} \nFN = {FN}')

plot_against_index = 4

if (plot_against_index != -1):
    plot_against_data = [None] * len(holdout_data)
    risk_data = [None] * len(holdout_data_result)

    for i in range(len(holdout_data)):
        risk_data[i] = risk[i]
        plot_against_data[i] = holdout_data[i][plot_against_index]

    plot_against_data, risk_data = zip(*sorted(zip(plot_against_data, risk_data)))

    plot_against_data, risk_data = (list(t) for t in zip(*sorted(zip(plot_against_data, risk_data))))

    # plt.figure()
    # plt.plot(plot_against_data, risk_data, 'ro', label='Default Risk')
    # plt.legend()
    # plt.show()

    xy = np.vstack([plot_against_data, risk_data])
    z = gaussian_kde(xy)(xy)

    fig, ax = plt.subplots()
    ax.scatter(plot_against_data, risk_data, c=z, s=100, edgecolor='')

    plt.xlabel(COLUMNS[plot_against_index])
    plt.ylabel('Default')

    plt.show()
