import pandas as pd
import numpy as np
from scipy import stats
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_recall_curve, recall_score, classification_report, auc, roc_curve
from sklearn.metrics import precision_recall_fscore_support, f1_score
from sklearn.preprocessing import StandardScaler
from pylab import rcParams
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras import regularizers
import time
import os

# set sementes aleatórias e porcentagem dos dados de teste
RANDOM_SEED = 314
TEST_PCT = 0.2

# set estilo do grafico 
rcParams['figure.figsize'] = 14, 8.7 # Golden Mean
LABELS = ['Normal', 'Fraud']
col_list = ['denim blue', 'scarlet']
sns.set(style='white', font_scale=1.75, palette=sns.xkcd_palette(col_list),color_codes=False)

df = pd.read_csv('files/creditcard.csv')
print(df.head(n=10))
print(df.shape)

# check para ver se algum valor é nulo
print(df.isnull().values.any())

# vamos contar quantos dados são Fraudes e quantos dados são normais
print(pd.value_counts(df['Class'], sort=True))

# vamos mostrar como os dados são desbalanceados, ou seja
# existe mais informação de um do que outro
# count_classes = pd.value_counts(df['Class'], sort=True)
# count_classes.plot(kind='bar', rot=0)
# plt.xticks(range(2), LABELS)
# plt.title('Números para a observação')
# plt.xlabel('Classes')
# plt.ylabel('Números de observações')
# plt.show()

# dividindo os dados em classes
normal_df = df[df.Class == 0]
fraud_df = df[df.Class == 1]

print(fraud_df.Amount.describe())

# mostrarei aqui a comparacao entre normal e fraude por valores acima de 200 usd
# bins = np.linspace(200, 2500, 100)
# plt.hist(normal_df.Amount, bins, alpha=1, density=True, label='Normal')
# plt.hist(fraud_df.Amount, bins, alpha=0.6, density=True, label='Fraude')
# plt.legend(loc='upper right')
# plt.title('Comparações de valores com transações acima de 200 dólares')
# plt.xlabel('Valores de Transações (USD)')
# plt.ylabel('Porcentagem de transações')
# plt.show()

# mostrarei aqui a comparacao entre normal e fraude por 48 horas
bins = np.linspace(0, 48, 48)
# plt.hist((normal_df.Time/(60*60)), bins, alpha=1, density=True, label='Normal')
# plt.hist((fraud_df.Time/(60*60)), bins, alpha=0.6, density=True, label='Fraude')
# plt.legend(loc='upper right')
# plt.title('Comparações de valores por hora')
# plt.xlabel('Tempo de transação medido da primeira transação no conjunto de dados (horas)')
# plt.ylabel('Porcentagem de transações')
# plt.show()
time_init = time.time()
# vamos normalizar os dados
df_norm = df
df_norm['Time'] = StandardScaler().fit_transform(df_norm['Time'].values.reshape(-1, 1))
df_norm['Amount'] = StandardScaler().fit_transform(df_norm['Amount'].values.reshape(-1, 1))


train_x, test_x =  train_test_split(df_norm, test_size=TEST_PCT, random_state=RANDOM_SEED)
train_x = train_x[train_x.Class == 0] # pega as transações normais
train_x = train_x.drop(['Class'], axis=1) # retira a coluna Class

test_y = test_x['Class'] # salva a coluna Class para test
test_x = test_x.drop(['Class'], axis=1)

train_x = train_x.values # tranform in ndarray
test_x = test_x.values

print(train_x.shape)

nb_epochs = 1000
batch_size = 128
input_dim = train_x.shape[1] # numeros de colunas == 30
encoding_dim = 30
hidden_dim = int(encoding_dim/2) # == 7
learning_rate = 1e-7

input_layer = Input(shape=(input_dim, ))
encoder = Dense(encoding_dim, activation='tanh', activity_regularizer=regularizers.l1(learning_rate))(input_layer)
encoder = Dense(hidden_dim, activation='relu')(encoder)
decoder = Dense(hidden_dim, activation='tanh')(encoder)
decoder = Dense(input_dim, activation='relu')(decoder)
autoencoder = Model(inputs=input_layer, outputs=decoder)

autoencoder.compile(metrics=['accuracy'], loss='mean_squared_error', optimizer='adam')
cp = ModelCheckpoint(filepath='auto_encoder.h5', save_best_only=True, verbose=0)
tb = TensorBoard(log_dir='logs/', histogram_freq=0, write_graph=True, write_images=True)
# autoencoder = load_model('auto_encoder.h5')
history = autoencoder.fit(train_x, train_x, epochs=nb_epochs, batch_size=batch_size, shuffle=True, 
validation_data=(test_x, test_x), verbose=1, callbacks=[cp, tb]).history
final = (time.time() - time_init)/60

# veromos agora acuracia
plt.plot(history['acc'], linewidth=2, label='Train')
# plt.plot(history['val_acc'], linewidth=2, label='Test')
plt.legend(loc='upper right')
plt.title('Acuracia em epocas')
plt.ylabel('Acurácia')
plt.xlabel('Épocas')
plt.show()

# veremos agora a taxa de perda e acuracia
plt.plot(history['loss'], linewidth=2, label='Train')
plt.plot(history['val_loss'], linewidth=2, label='Test')
plt.legend(loc='upper right')
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epocas')
plt.show()

test_x_prediction = autoencoder.predict(test_x)
mse = np.mean(np.power(test_x - test_x_prediction, 2), axis=1)
error_df = pd.DataFrame({'Reconstruction_error': mse, 'True_class': test_y})
print(error_df.describe())

precision_rt, recall_rt, threshold_rt = precision_recall_curve(error_df.True_class, error_df.Reconstruction_error)
plt.plot(recall_rt, precision_rt, linewidth=5, label='Precision-Recall curve')
plt.title('Recall vs Precision')
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.show()

plt.plot(threshold_rt, precision_rt[1:], label="Precision",linewidth=5)
plt.plot(threshold_rt, recall_rt[1:], label="Recall",linewidth=5)
plt.title('Precision and recall for different threshold values')
plt.xlabel('Threshold')
plt.ylabel('Precision/Recall')
plt.legend()
plt.show()

threshold_fixed = 5
groups = error_df.groupby('True_class')
fig, ax = plt.subplots()
[ax.plot(group.index, group.Reconstruction_error, marker='o', ms=3.5, linestyle='', label='Fraud' if name == 1 else 'Normal') for name, group in groups]
ax.hlines(threshold_fixed, ax.get_xlim()[0], ax.get_xlim()[1], colors='r', zorder=100, label='Threshold')
ax.legend()
plt.title("Reconstruction error for different classes")
plt.ylabel("Reconstruction error")
plt.xlabel("Data point index")
plt.show()

pred_y = [1 if e > threshold_fixed else 0 for e in error_df.Reconstruction_error.values]
conf_matrix = confusion_matrix(error_df.True_class, pred_y)

plt.figure(figsize=(12, 12))
sns.heatmap(conf_matrix, xticklabels=LABELS, yticklabels=LABELS, annot=True, fmt="d");
plt.title("Confusion matrix")
plt.ylabel('True class')
plt.xlabel('Predicted class')
plt.show()

print('\n')
print('===================Informações do treino=======================')
print(f'''
    Total de Linhas: {df.shape[0]}
    Total de Colunas: {df.shape[1]}
    Dados de treinamento(80%): {int(df.shape[0] * 0.8)}
    Dados de Teste: {int(df.shape[0]*0.2)}
    Épocas: {nb_epochs}
    Tempo de Treinamento: {int(final)} Minutos
''')
print(f'Treinamento concluído com sucesso!')

